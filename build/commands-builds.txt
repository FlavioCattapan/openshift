# Create a new BuildConfig from a Git repository URL
oc new-build https://gitlab.com/practical-openshift/hello-world.git

# Get more information about the bui
oc get -o yaml buildconfig/hello-world

# See builds that have run
oc get build

oc get pod

# See all of your pods
oc get pods

# Get logs for a single build
oc logs -f build/hello-world-1

# Get logs for the latest build for a BuildConfig
# This is the best way (usually)
oc logs -f bc/hello-world

# Start a build for an existing BuildConfig
oc start-build bc/hello-world

# Watch the pods
oc get pods --watch

# Builds result in an update to an ImageStream
oc describe is/hello-world

# Cancel a running build
oc cancel-build bc/hello-world

# Get the secret token
oc get -o yaml buildconfig/hello-world

# Export the secret as a variable
export GENERIC_SECRET=nVMzeUDerRpkTGXC85Bi

# Get the webhook URL
oc describe buildconfig/hello-world

# Copy the webhook URL and replace <secret> with

https://192.168.99.100:8443/apis/build.openshift.io/v1/namespaces/labs/buildconfigs/hello-world/webhooks/$GENERIC_SECRET/generic
 $GENERIC_SECRET
curl -X POST -k <webhook URL with secret replaced with $GENERIC_SECRET>


curl -X POST -k https://192.168.99.100:8443/apis/build.openshift.io/v1/namespaces/labs/buildconfigs/hello-world/webhooks/$GENERIC_SECRET/generic
